package mayk.com.popularmoviesstageone.json;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import mayk.com.popularmoviesstageone.MainActivity;
import mayk.com.popularmoviesstageone.Movies;
import mayk.com.popularmoviesstageone.Utils;

import static mayk.com.popularmoviesstageone.Static.API_KEY;
import static mayk.com.popularmoviesstageone.Static.API_URL;
import static mayk.com.popularmoviesstageone.Static.BASE_URL;
import static mayk.com.popularmoviesstageone.Static.GET;
import static mayk.com.popularmoviesstageone.Static.POPULARITY;
import static mayk.com.popularmoviesstageone.Static.SIZE;
import static mayk.com.popularmoviesstageone.Static.SORTING;

public class JSONMovieDetailsDownloader extends AsyncTask<Void,Void,String> {

    @SuppressLint("StaticFieldLeak")
    private Context context;

    public JSONMovieDetailsDownloader(Context context) {
        this.context = context;
    }
//    https://api.themoviedb.org/3/movie/top_rated?api_key=0966b67be7be8aea04b2b66a5e193dfa
    @Override
    protected String doInBackground(Void... voids) {
        String sorting = Utils.getPreferences(SORTING, context);
        if (TextUtils.isEmpty(sorting))
        {
            Utils.setPreferences(SORTING, POPULARITY, context);
        }

        Uri builtUri = Uri.parse(API_URL).buildUpon()
                .appendPath(sorting)
                .appendQueryParameter("api_key", API_KEY)
                .build();

        String response;
        try {
            response  = download(builtUri);
            return response;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    protected void onPostExecute(String response) {
        if (response != null) {
            parse(response);
        }
    }

    public static String download(Uri jsonURL) {

        String json;

        try {
            URL url = new URL(jsonURL.toString());
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod(GET);
            httpURLConnection.connect();


            InputStream inputStream = httpURLConnection.getInputStream();
            StringBuffer stringBuffer = new StringBuffer();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line + "\n");
            }
            json = stringBuffer.toString();
        } catch (IOException e) {
            return null;
        }

        return json;
    }

    public static void parse(String jsonString)
    {
        MainActivity.moviesList.clear();
        try
        {
            if(!TextUtils.isEmpty(jsonString)){


                JSONObject mObject = new JSONObject(jsonString);
                JSONArray mArray = mObject.getJSONArray("results");

                for (int i=0;i<mArray.length();i++)
                {
                    JSONObject movie = mArray.getJSONObject(i);
                    Movies movie1 = new Movies();
                    movie1.setTitle(movie.getString("title"));
                    movie1.setPlot(movie.getString("overview"));
                    movie1.setDate(movie.getString("release_date"));
                    movie1.setRating(movie.getString("vote_average"));
                    movie1.setId(movie.getString("id"));
                    String poster_url = (movie.getString("poster_path"));
                    movie1.setPosterUrl(BASE_URL+ SIZE+ poster_url);
                    MainActivity.moviesList.add(movie1);
                    MainActivity.moviesPosterAdapter.notifyDataSetChanged();
                }

            }

        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }
}