package mayk.com.popularmoviesstageone;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {

    private List<Movies> reviewsLists;
    private Context context;

    public ReviewsAdapter(Context context, List<Movies> reviewsLists) {
        this.context = context;
        this.reviewsLists= reviewsLists;
    }

    @NonNull
    @Override
    public ReviewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_reviews, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewsAdapter.ViewHolder holder, final int position) {
        final Movies movies = reviewsLists.get(position);
        holder.reviewAuthor.setText(movies.getReviewsAuthor());
        holder.reviewContent.setText(movies.getReviewsContent());
    }

    @Override
    public int getItemCount() {
        return reviewsLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView reviewContent;
        public TextView reviewAuthor;

        public ViewHolder(View itemView) {
            super(itemView);
            reviewAuthor = itemView.findViewById(R.id.review_author);
            reviewContent = itemView.findViewById(R.id.review_content);
        }

    }
}
