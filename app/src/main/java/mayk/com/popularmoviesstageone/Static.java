package mayk.com.popularmoviesstageone;

public class Static {
//https://api.themoviedb.org/3/movie/top_rated?api_key=<<api_key>>

    //Imdb
    public static String API_KEY = "0966b67be7be8aea04b2b66a5e193dfa";
    public static String BASE_URL = "http://image.tmdb.org/t/p/";
    public static String SIZE = "w185";
    static public String API_URL = "http://api.themoviedb.org/3/movie";
    public static String POPULARITY  = "popular";
    public static String AVERAGE = "top_rated";
    public static String SORTING = "SORTING";
    public static String YOUTUBE_THUMBNAIL = "http://img.youtube.com/vi/";
    public static String DEFAULT_THUMBNAIL = "/hqdefault.jpg";
    public static String YOUTUBE_LINK = "https://www.youtube.com/watch?v=";

    //Intent Extras
    public static String POSTER_URL = "poster_url";
    public static String TITLE = "title";
    public static String PLOT = "plot";
    public static String DATE = "date";
    public static String RATING = "rating";
    public static String ID = "id";

    // Json
    public static String GET = "GET";
    public static String ERROR = "Error ";
}
