package mayk.com.popularmoviesstageone;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

import mayk.com.popularmoviesstageone.data.FavoritesContract;
import mayk.com.popularmoviesstageone.json.JSONMovieDetailsDownloader;

import static mayk.com.popularmoviesstageone.Static.AVERAGE;
import static mayk.com.popularmoviesstageone.Static.BASE_URL;
import static mayk.com.popularmoviesstageone.Static.POPULARITY;
import static mayk.com.popularmoviesstageone.Static.SIZE;
import static mayk.com.popularmoviesstageone.Static.SORTING;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor>  {

    private RecyclerView recyclerView;
    public static MoviesPosterAdapter moviesPosterAdapter;
    public static ArrayList<Movies> moviesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moviesList = new ArrayList<>();
        moviesPosterAdapter = new MoviesPosterAdapter(this, moviesList);
        recyclerView = findViewById(R.id.recycler_view_posters);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),3);
        recyclerView.setLayoutManager(gridLayoutManager);
        findViewById(R.id.highest_rated).setOnClickListener(this);
        findViewById(R.id.most_popular).setOnClickListener(this);
        findViewById(R.id.favorites).setOnClickListener(this);
        new JSONMovieDetailsDownloader(MainActivity.this).execute();

    }
    @NonNull
    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri NAME_URI = FavoritesContract.MovieEntry.CONTENT_URI;
        return new CursorLoader(this, NAME_URI, null,
                null, null, null);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader
            , Cursor mCursor) {
        if (mCursor != null) {
            mCursor.moveToPosition(-1);
            try {
                while (mCursor.moveToNext()) {
                    String id = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_ID));
                    String title = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_TITLE));
                    String overview = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_OVERVIEW));
                    String posterPath = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_POSTER));
//                    String posterUrl = BASE_URL+ SIZE+ posterPath;
                    String movieAvg = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_AVG));
                    Movies movie = new Movies(id, title, overview, posterPath, movieAvg);
                    MainActivity.moviesList.add(movie);
                    MainActivity.moviesPosterAdapter.notifyDataSetChanged();
                }
            } finally {
            }
        }
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader loader) {

    }

    @Override
    public void onClick(View v) {

        int i = v.getId();

        if(i == R.id.most_popular)
        {
            recyclerView.setAdapter(moviesPosterAdapter);
            Utils.setPreferences(SORTING, POPULARITY, MainActivity.this);
            new JSONMovieDetailsDownloader(MainActivity.this).execute();

        }
        if(i == R.id.highest_rated)
        {
            recyclerView.setAdapter(moviesPosterAdapter);
            Utils.setPreferences(SORTING, AVERAGE, MainActivity.this);
            new JSONMovieDetailsDownloader(MainActivity.this).execute();
        }
        if(i == R.id.favorites)
        {
            recyclerView.setAdapter(moviesPosterAdapter);
            moviesList.clear();
            getSupportLoaderManager().initLoader(1, null, this);
        }
    }
}
