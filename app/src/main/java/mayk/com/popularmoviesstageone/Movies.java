package mayk.com.popularmoviesstageone;

public class Movies {

    private String posterUrl, title, plot, date, rating, id, youtubeKey, reviewsAuthor, reviewsContent;

    public Movies() {
    }

    public Movies(String id, String title, String plot, String posterUrl, String rating) {
        this.posterUrl = posterUrl;
        this.title = title;
        this.plot = plot;
        this.rating = rating;
        this.id = id;
    }

    //    String id = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_ID));
//    String title = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_TITLE));
//    String overview = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_OVERVIEW));
//    String posterPath = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_POSTER));
//    String movieAvg = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_AVG));
    public Movies(String posterUrl, String title, String plot, String date, String rating, String id,
                  String youtubeKey, String reviewsAuthor, String reviewsContent) {
        this.posterUrl = posterUrl;
        this.title = title;
        this.plot = plot;
        this.date = date;
        this.rating = rating;
        this.id = id;
        this.youtubeKey= youtubeKey;
        this.reviewsAuthor= reviewsAuthor;
        this.reviewsContent= reviewsContent;
    }

    public String getReviewsAuthor() {
        return reviewsAuthor;
    }

    public void setReviewsAuthor(String reviewsAuthor) {
        this.reviewsAuthor = reviewsAuthor;
    }

    public String getReviewsContent() {
        return reviewsContent;
    }

    public void setReviewsContent(String reviewsContent) {
        this.reviewsContent = reviewsContent;
    }

    public String getYoutubeKey() {
        return youtubeKey;
    }

    public void setYoutubeKey(String youtubeKey) {
        this.youtubeKey = youtubeKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
