package mayk.com.popularmoviesstageone;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import mayk.com.popularmoviesstageone.data.FavoritesAdapter;
import mayk.com.popularmoviesstageone.data.FavoritesContract;
import mayk.com.popularmoviesstageone.data.FavoritesDbHelper;
import mayk.com.popularmoviesstageone.json.JSONMovieDetailsDownloader;
import mayk.com.popularmoviesstageone.json.JSONReviewsDownloader;
import mayk.com.popularmoviesstageone.json.JSONTrailersDownloader;

import static mayk.com.popularmoviesstageone.Static.DATE;
import static mayk.com.popularmoviesstageone.Static.ID;
import static mayk.com.popularmoviesstageone.Static.PLOT;
import static mayk.com.popularmoviesstageone.Static.POSTER_URL;
import static mayk.com.popularmoviesstageone.Static.RATING;
import static mayk.com.popularmoviesstageone.Static.TITLE;

public class MoviesDetailActivity extends AppCompatActivity {

    private FavoritesAdapter mAdapter;
    private SQLiteDatabase mDb;
    private ImageView posterView;
    private TextView titleView;
    private TextView plotView;
    private TextView dateView;
    private TextView ratingView;
    public static ArrayList<Movies> trailersList;
    public static ArrayList<Movies> reviewsList;
    private RecyclerView trailersRecyclerView;
    public static TrailersAdapter trailersAdapter;
    public static ReviewsAdapter reviewsAdapter;
    private RecyclerView reviewsRecyclerView;
    private String id;
    private Button unFav;
    private Button fav;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movies_detail_layout);
        FavoritesDbHelper dbHelper = new FavoritesDbHelper(this);
        mDb = dbHelper.getWritableDatabase();
        posterView = findViewById(R.id.movies_image);
        titleView = findViewById(R.id.original_title);
        plotView = findViewById(R.id.plot);
        dateView = findViewById(R.id.release_date);
        ratingView = findViewById(R.id.rating);
        trailersRecyclerView = findViewById(R.id.recycler_view_trailers);
        reviewsRecyclerView = findViewById(R.id.recycler_view_reviews);

        trailersList = new ArrayList<>();
        trailersAdapter = new TrailersAdapter(this, trailersList);
        trailersRecyclerView.setHasFixedSize(true);
        LinearLayoutManager trailersLManager = new LinearLayoutManager(this);
        trailersLManager.setOrientation(LinearLayoutManager.VERTICAL);
        trailersRecyclerView.setLayoutManager(trailersLManager);
        trailersRecyclerView.setAdapter(trailersAdapter);

        LinearLayoutManager reviewsLManager = new LinearLayoutManager(this);
        reviewsLManager.setOrientation(LinearLayoutManager.VERTICAL);
        reviewsList = new ArrayList<>();
        reviewsAdapter = new ReviewsAdapter(this, reviewsList);
        reviewsRecyclerView.setHasFixedSize(true);
        reviewsRecyclerView.setLayoutManager(reviewsLManager);
        reviewsRecyclerView.setAdapter(reviewsAdapter);
        checkIfIsInFavorites();
        new JSONTrailersDownloader(this).execute();
        new JSONReviewsDownloader(this).execute();
        Intent i=this.getIntent();
        final String posterUrl=i.getExtras().getString(POSTER_URL);
        final String title=i.getExtras().getString(TITLE);
        final String plot=i.getExtras().getString(PLOT);
        final String date=i.getExtras().getString(DATE);
        final String rating=i.getExtras().getString(RATING);
        id = i.getExtras().getString(ID);
        Picasso.with(MoviesDetailActivity.this).load(posterUrl).into(posterView);
        titleView.setText(title);
        plotView.setText(plot);
        dateView.setText(date);
        ratingView.setText(rating);
        unFav= findViewById(R.id.favorite_delete);
        fav = findViewById(R.id.favorite);
        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fav.setVisibility(View.GONE);
                unFav.setVisibility(View.VISIBLE);
                saveToFavorites(id, posterUrl, title, plot, date, rating);
            }
        });
        unFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unFav.setVisibility(View.GONE);
                deleteFromFavorites();
                fav.setVisibility(View.VISIBLE);
            }
        });
    }

    private void checkIfIsInFavorites() {
        Cursor isInFavorites=  getContentResolver().query(FavoritesContract.MovieEntry.CONTENT_URI, null, id, null, null);
        if (isInFavorites!=null){
            if (isInFavorites.getCount()!=0) {
                fav.setVisibility(View.GONE);
                unFav.setVisibility(View.VISIBLE);
            }
        }
    }

    private void deleteFromFavorites() {
        getContentResolver().delete(FavoritesContract.MovieEntry.CONTENT_URI,
                FavoritesContract.MovieEntry.MOVIE_ID + "=?", new String[]{id});
    }

    private void saveToFavorites(String id, String posterUrl, String title, String plot, String date, String rating) {
        ContentValues cv = new ContentValues();
        cv.put(FavoritesContract.MovieEntry.MOVIE_ID, id);
        cv.put(FavoritesContract.MovieEntry.MOVIE_TITLE, title);
        cv.put(FavoritesContract.MovieEntry.MOVIE_OVERVIEW, plot);
        cv.put(FavoritesContract.MovieEntry.MOVIE_RELEASE_DATE, date);
        cv.put(FavoritesContract.MovieEntry.MOVIE_AVG, rating);
        cv.put(FavoritesContract.MovieEntry.MOVIE_POSTER, posterUrl);
        getContentResolver().insert(FavoritesContract.MovieEntry.CONTENT_URI, cv);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new JSONMovieDetailsDownloader(this).execute();
    }
}
