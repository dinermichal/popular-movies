package mayk.com.popularmoviesstageone;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import static mayk.com.popularmoviesstageone.Static.DATE;
import static mayk.com.popularmoviesstageone.Static.ID;
import static mayk.com.popularmoviesstageone.Static.PLOT;
import static mayk.com.popularmoviesstageone.Static.POSTER_URL;
import static mayk.com.popularmoviesstageone.Static.RATING;
import static mayk.com.popularmoviesstageone.Static.TITLE;

public class MoviesPosterAdapter extends RecyclerView.Adapter<MoviesPosterAdapter.ViewHolder> {

    private List<Movies> moviesLists;
    private Context context;

    public MoviesPosterAdapter(Context context, List<Movies> moviesLists) {
        this.context = context;
        this.moviesLists=moviesLists;
    }

    @NonNull
    @Override
    public MoviesPosterAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_posters, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesPosterAdapter.ViewHolder holder, final int position) {
        final Movies movies = moviesLists.get(position);
        Picasso.with(context)
                .load(movies.getPosterUrl())
                .into(holder.poster);
        holder.poster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Movies movies1 = moviesLists.get(position);
                Intent detailIntent = new Intent (v.getContext(), MoviesDetailActivity.class);
                detailIntent.putExtra(POSTER_URL, movies1.getPosterUrl());
                detailIntent.putExtra(TITLE, movies1.getTitle());
                detailIntent.putExtra(PLOT, movies1.getPlot());
                detailIntent.putExtra(RATING, movies1.getRating());
                detailIntent.putExtra(DATE, movies1.getDate());
                detailIntent.putExtra(ID, movies1.getId());
                Utils.setPreferences(ID, movies1.getId(), v.getContext());
                v.getContext().startActivity(detailIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView poster;

        public ViewHolder(View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.poster);
        }

    }
}
