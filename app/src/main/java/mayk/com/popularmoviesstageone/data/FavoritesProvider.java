package mayk.com.popularmoviesstageone.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class FavoritesProvider extends ContentProvider {
    public static final int FAV_MOVIE = 101;
    private static UriMatcher sUriMatcher = buildUriMatcher();
    private FavoritesDbHelper mDbHelper;

    public static UriMatcher buildUriMatcher(){
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        final String authority = FavoritesContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, FavoritesContract.PATH_MOVIE, FAV_MOVIE);


        return matcher;
    }

    @Override
    public boolean onCreate() {
        mDbHelper = new FavoritesDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor;
        switch (sUriMatcher.match(uri)){
            case FAV_MOVIE:{
                SQLiteDatabase db = mDbHelper.getReadableDatabase();
                cursor = db.query(FavoritesContract.MovieEntry.TABLE_NAME,
                        projection,selection,selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri returnUri = null;

        switch (sUriMatcher.match(uri)){
            case FAV_MOVIE:{
                SQLiteDatabase db = mDbHelper.getWritableDatabase();
                long id = db.insert(FavoritesContract.MovieEntry.TABLE_NAME,null,values);
                if (id>0){
                    returnUri =  FavoritesContract.MovieEntry.buildMovieUri(id);
                }
                break;
            }
            default:{
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        switch (sUriMatcher.match(uri)){
            case FAV_MOVIE:{
                SQLiteDatabase db = mDbHelper.getWritableDatabase();
                return db.delete(FavoritesContract.MovieEntry.TABLE_NAME,selection,selectionArgs);
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}