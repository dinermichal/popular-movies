package mayk.com.popularmoviesstageone.data;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import mayk.com.popularmoviesstageone.R;

public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.FavoritesViewHolder> {
    // Holds on to the cursor to display the waitlist
    private Cursor mCursor;
    private Context mContext;

    /**
     * Constructor using the context and the db cursor
     *
     * @param context the calling context/activity
     * @param cursor  the db cursor with waitlist data to display
     */
    public FavoritesAdapter(Context context, Cursor cursor) {
        this.mContext = context;
        this.mCursor = cursor;
    }

    @Override
    public FavoritesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Get the RecyclerView item layout
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.movie_posters, parent, false);
        return new FavoritesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FavoritesViewHolder holder, int position) {
        // Move the mCursor to the position of the item to be displayed
        if (!mCursor.moveToPosition(position))
            return; // bail if returned null

//        // Update the view holder with the information needed to display
//        String movieId = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_ID));
//
        String id = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_ID));
//        String title = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_TITLE));
//        String overview = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_OVERVIEW));
//        String posterPath = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_POSTER));
//        String movieAvg = mCursor.getString(mCursor.getColumnIndex(FavoritesContract.MovieEntry.MOVIE_AVG));
        // COMPLETED (7) Set the tag of the itemview in the holder to the id
        holder.itemView.setTag(id);
//        String id, String posterUrl, String title, String plot, String date, String rating
    }

    public void swapCursor(Cursor newCursor) {
        // COMPLETED (16) Inside, check if the current cursor is not null, and close it if so
        // Always close the previous mCursor first
        if (mCursor != null) mCursor.close();
        // COMPLETED (17) Update the local mCursor to be equal to  newCursor
        mCursor = newCursor;
        // COMPLETED (18) Check if the newCursor is not null, and call this.notifyDataSetChanged() if so
        if (newCursor != null) {
            // Force the RecyclerView to refresh
            this.notifyDataSetChanged();
        }
    }
    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }

    class FavoritesViewHolder extends RecyclerView.ViewHolder {
        public ImageView poster;

        public FavoritesViewHolder(View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.poster);
        }
    }
}
