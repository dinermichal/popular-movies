package mayk.com.popularmoviesstageone;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import static mayk.com.popularmoviesstageone.Static.DEFAULT_THUMBNAIL;
import static mayk.com.popularmoviesstageone.Static.YOUTUBE_LINK;
import static mayk.com.popularmoviesstageone.Static.YOUTUBE_THUMBNAIL;

public class TrailersAdapter extends RecyclerView.Adapter<TrailersAdapter.ViewHolder> {

    private List<Movies> trailersLists;
    private Context context;

    public TrailersAdapter(Context context, List<Movies> trailersLists) {
        this.context = context;
        this.trailersLists= trailersLists;
    }

    @NonNull
    @Override
    public TrailersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_trailers, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TrailersAdapter.ViewHolder holder, final int position) {
        final Movies movies = trailersLists.get(position);
        Picasso.with(context)
                .load(YOUTUBE_THUMBNAIL + movies.getYoutubeKey() + DEFAULT_THUMBNAIL)
                .into(holder.trailer);
        holder.trailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(YOUTUBE_LINK + movies.getYoutubeKey())));
            }
        });
    }

    @Override
    public int getItemCount() {
        return trailersLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView trailer;

        public ViewHolder(View itemView) {
            super(itemView);
            trailer = itemView.findViewById(R.id.trailer);
        }

    }
}
